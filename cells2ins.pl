#!/usr/bin/env perl
use strict;
use warnings;

my @grid;
my $length = 0;
my $y = 0;
while (<>) {
    next if $_ =~ /^!/;

    my $x = 0;
    $_ =~ s/\s+$//;
    $grid[$y][$x++] = $_ eq 'O' ? 1 : 0 for split //, $_;

    $length = $x if $x > $length;
    $y++;
}

my $height = scalar @grid;
for my $l (@grid) {
    @$l[$_] = 0 for scalar @$l .. $length - 1;
}

print "$height $length\n";
print "@$_\n" for @grid;
print "100\n0\n"
