# Jeu de la vie

## Dépendances

- Compilateur C99
- `make`

Dépendances optionnelles :

- Doxygen pour la documentation
- `pdflatex` pour générer un pdf de la documentation
- `perl` pour les convertisseurs de format de motif
- GTK+ 3 et `pkg-config` pour l'interface graphique

## Compilation

```
make
```

### Interface graphique

```
make gui
```

### Génération de la documentation

```
make doc
```

Pour générer le pdf de la documentation :

```
cd latex
make
```

### Tests unitaires

```
make test
```

### Code golf

```
make golf
```

Le code golf est une pratique qui consiste à implémenter un algorithme en
produisant le programme le plus court possible.

Ici nous avons implémenté le jeu de la vie décrit par les consignes
(lecture des motifs, matrice finie et torique, affichage terminal)
en 373 caractères (`make count` pour compter en enlevant tous les caractères
inutiles à la compilation).

## Utilisation

Utilisation basique :
```
./gol < motifs/motif.txt
```

Le programme gère les arguments.
Pour avoir l'aide :
```
./gol --help
```

Exemple :
```
./gol -d 100 -v "*" -m " " motifs/motif.txt
```

### Interface graphique

```
./gui
```
Ensuite cliquer sur le bouton en haut à gauche pour ouvrir un motif.

### Code golf

```
./golf < motifs/motif.txt
```

### Convertisseurs

Deux convertisseurs de format de motif sont disponnibles :
`cells2ins.pl` et `rle2ins.pl` (qui est un peu plus complet).

```
./cells2ins.pl motif.cells > motif.txt
```
```
curl https://www.conwaylife.com/patterns/gabrielsp138.rle | ./rle2ins.pl -x 7 -y 7 -h 35 -w 35 -g 200 > motifs/p138.txt
```

## Structure du projet

- `gol.c` et `gol.h` : fonctions relatives au jeu de la vie
- `gol_test.c` : tests unitaires de ces fonctions
- `main.c` et `config.h` : interface terminal
- `gui.c` : interface graphique
- `golf.c` et `golf_commente.c` : tentative d'implémentation avec
  le moins de caractères possible et son explication
- `rapport.tex` et `rapport.bib` : rapport écrit en LaTeX
- `motifs` : dossier où se trouvent les motifs
- `cells2ins.pl` et `rle2ins.pl` : convertisseurs de format de motif
- `surprise.py` : c'est une surprise à exécuter
- `Doxyfile` : configuration Doxygen
- `flake.nix`, `flake.lock`, `.envrc` : configuration Nix/direnv
