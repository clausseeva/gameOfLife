{
  description = "A very basic flake";

  inputs.flake-utils.url = "github:numtide/flake-utils";

  outputs = { self, nixpkgs, flake-utils }:
    flake-utils.lib.eachDefaultSystem (system: let
      pkgs = nixpkgs.legacyPackages.${system};
    in {
      devShell = pkgs.mkShell {
        nativeBuildInputs = with pkgs; [
          gnumake
          gcc
          gdb
          ccls
          doxygen
          texlive.combined.scheme-full
          gtk3
          pkg-config
          perl
          python3
        ];

        shellHook = ''
          echo "${pkgs.gcc}/bin/gcc" > ./.ccls
          ${pkgs.pkg-config}/bin/pkg-config --cflags gtk+-3.0 | tr ' ' '\n' >> ./.ccls
        '';
      };
    });
}
