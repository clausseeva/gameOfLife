#include <gtk/gtk.h>

#include "gol.h"

static Grille *g;
static GtkWidget *da;
static GtkWidget *sb;
static guint to;

static gboolean draw_grille(GtkWidget *w, cairo_t *cr, gpointer data) {
    int height = gtk_widget_get_allocated_height(w);
    int width = gtk_widget_get_allocated_width(w);
    GtkStyleContext *context = gtk_widget_get_style_context(w);
    GdkRGBA color;

    gtk_render_background(context, cr, 0, 0, width, height);

    if (g == NULL)
        return FALSE;

    double prop = MIN((double)width / g->largeur, (double)height / g->hauteur);
    double dh = prop * g->hauteur;
    double dw = prop * g->largeur;
    double oy = ((double)height - dh) / 2;
    double ox = ((double)width - dw) / 2;
    gtk_style_context_get_color(context, gtk_style_context_get_state(context), &color);
    gdk_cairo_set_source_rgba(cr, &color);

    for (int i = 0; i <= g->largeur; i++) {
        double x = ox + prop * i;
        cairo_move_to(cr, x, oy);
        cairo_line_to(cr, x, oy + dh);
    }

    for (int i = 0; i <= g->hauteur; i++) {
        double y = oy + prop * i;
        cairo_move_to(cr, ox, y);
        cairo_line_to(cr, ox + dw, y);
    }
    cairo_stroke(cr);

    for (int j = 0; j < g->hauteur; j++)
        for (int i = 0; i < g->largeur; i++)
            if (*g->acceder(g, j, i) & 1)
                cairo_rectangle(cr, ox + prop * i, oy + prop * j, prop, prop);

    cairo_fill(cr);

    return FALSE;
}

static void ouvre_fichier(GtkWidget *w, gpointer data) {
    GtkWindow *win = GTK_WINDOW(data);
    GtkWidget *dialog = gtk_file_chooser_dialog_new("Ouvrir motif", win,
                                                    GTK_FILE_CHOOSER_ACTION_OPEN,
                                                    "Annuler", GTK_RESPONSE_CANCEL,
                                                    "Ouvrir", GTK_RESPONSE_ACCEPT,
                                                    NULL);

    int res = gtk_dialog_run(GTK_DIALOG(dialog));
    if (res == GTK_RESPONSE_ACCEPT) {
        char *filename = gtk_file_chooser_get_filename(GTK_FILE_CHOOSER(dialog));
        gtk_widget_destroy(dialog);

        FILE *fp = fopen(filename, "r");
        g_assert(fp != NULL);

        g = grille_init_fichier(fp);
        if (g == NULL) {
            GtkWidget *err_dialog =
                gtk_message_dialog_new(win,
                                       GTK_DIALOG_DESTROY_WITH_PARENT,
                                       GTK_MESSAGE_ERROR, GTK_BUTTONS_CLOSE,
                                       "Impossible d'iterpréter le fichier %s.",
                                       filename);
            gtk_dialog_run(GTK_DIALOG(err_dialog));
            gtk_widget_destroy(err_dialog);
        }

        fclose(fp);
        g_free(filename);

        gtk_widget_queue_draw(da);
    } else {
        gtk_widget_destroy(dialog);
    }
}

static gboolean tour_suivant_callback(gpointer data) {
    if (g == NULL)
        return TRUE;
    tour_suivant(g);
    gtk_widget_queue_draw(da);
    return TRUE;
}

static void depart(GtkWidget *w, gpointer data) {
    if (to == 0) {
        int val = gtk_spin_button_get_value_as_int(GTK_SPIN_BUTTON(sb));
        to = g_timeout_add(val, tour_suivant_callback, NULL);
    }
}

static void arret(GtkWidget *w, gpointer data) {
    if (to != 0) {
        g_source_remove(to);
        to = 0;
    }
}

static void change_status(GtkWidget *w, gpointer data) {
    if (gtk_toggle_tool_button_get_active(GTK_TOGGLE_TOOL_BUTTON(w)))
        depart(NULL, NULL);
    else
        arret(NULL, NULL);
}

static void change_vitesse(GtkSpinButton *sb, gpointer data) {
    if (to != 0) {
        arret(NULL, NULL);
        depart(NULL, NULL);
    }
}

static void activate(GtkApplication *app, gpointer data) {
    GtkWidget *win = gtk_application_window_new(app);
    gtk_window_set_title(GTK_WINDOW(win), "Jeu de la vie");
    gtk_window_set_default_size(GTK_WINDOW(win), 200, 200);

    GtkWidget *box = gtk_box_new(GTK_ORIENTATION_VERTICAL, 0);
    gtk_container_add(GTK_CONTAINER(win), box);

    GtkWidget *tb = gtk_toolbar_new();
    gtk_box_pack_start(GTK_BOX(box), tb, FALSE, FALSE, 0);

    GtkToolItem *bouton_ovrir = gtk_tool_button_new(NULL, NULL);
    gtk_tool_button_set_icon_name(GTK_TOOL_BUTTON(bouton_ovrir), "document-open");
    g_signal_connect(bouton_ovrir, "clicked", G_CALLBACK(ouvre_fichier), win);
    gtk_toolbar_insert(GTK_TOOLBAR(tb), bouton_ovrir, -1);

    GtkToolItem *separator = gtk_separator_tool_item_new();
    gtk_toolbar_insert(GTK_TOOLBAR(tb), separator, -1);

    GtkToolItem *bouton_play = gtk_toggle_tool_button_new();
    gtk_tool_button_set_icon_name(GTK_TOOL_BUTTON(bouton_play), "media-playback-start");
    g_signal_connect(bouton_play, "toggled", G_CALLBACK(change_status), NULL);
    gtk_toolbar_insert(GTK_TOOLBAR(tb), bouton_play, -1);

    GtkAdjustment *adjustment = gtk_adjustment_new(100, 0, 10000, 1, 5, 0);
    sb = gtk_spin_button_new(adjustment, 1, 0);
    g_signal_connect(sb, "value-changed", G_CALLBACK(change_vitesse), NULL);
    GtkToolItem *item_vitesse = gtk_tool_item_new();
    gtk_container_add(GTK_CONTAINER(item_vitesse), sb);
    gtk_toolbar_insert(GTK_TOOLBAR(tb), item_vitesse, -1);

    da = gtk_drawing_area_new();
    g_signal_connect(da, "draw", G_CALLBACK(draw_grille), NULL);
    gtk_box_pack_start(GTK_BOX(box), da, TRUE, TRUE, 0);

    gtk_widget_show_all(win);
}

int main(int argc, char **argv) {
    GtkApplication *app = gtk_application_new("org.test", G_APPLICATION_FLAGS_NONE);
    g_signal_connect(app, "activate", G_CALLBACK(activate), NULL);

    int status = g_application_run(G_APPLICATION(app), argc, argv);
    g_object_unref(app);

    return status;
}
