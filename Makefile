PROG := gol
CC := cc
CFLAGS := -Wall -Wextra -Wpedantic -Wconversion -g -O -std=c99
LDFLAGS :=

all: $(PROG)

$(PROG): main.o gol.o
	$(CC) -o $@ $^ $(LDFLAGS)

main.o: main.c gol.h config.h
	$(CC) -c -o $@ $< $(CFLAGS) -Wno-missing-field-initializers

gol.o: gol.c gol.h
	$(CC) -c -o $@ $< $(CFLAGS) -Wno-sign-conversion

test: gol_test
	@./gol_test

gol_test: gol_test.o
	$(CC) -o $@ $^ $(LDFLAGS)

gol_test.o: gol_test.c gol.h gol.c
	$(CC) -c -o $@ $< $(CFLAGS) -Wno-sign-conversion -std=gnu99

golf: golf.c
	$(CC) -o $@ $< -w -O -g

count: golf.c
	@tr -d ' \n' < $< | wc -c

gui: gui.o gol.o
	$(CC) -o $@ $^ $(LDFLAGS) $(shell pkg-config --libs gtk+-3.0)

gui.o: gui.c gol.h gtk_dep
	$(CC) -c -o $@ $< $(CFLAGS) -Wno-unused-parameter -Wno-sign-conversion $(shell pkg-config --cflags gtk+-3.0)

gtk_dep:
	@pkg-config gtk+-3.0 || ( echo "GTK+ 3 introuvable"; false )

doc:
	doxygen

rapport: rapport.pdf

rapport.pdf: rapport.tex rapport.bib
	pdflatex rapport
	biber rapport
	pdflatex rapport

clean:
	rm -f *.o $(PROG) gol_test golf gui
	rm -f *.pdf *.aux *.log *.bbl *.bcf *.blg *.run.xml

.PHONY: all doc rapport clean test gtk_dep
