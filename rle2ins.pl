#!/usr/bin/env perl
use v5.12;
use strict;
use warnings;
use experimental 'switch';

use Getopt::Long;

my $height;
my $width = 0;
my ($init_y, $init_x) = (0, 0);
my $gen = 100;

GetOptions (
    "height=i" => \$height,
    "width=i" => \$width,
    "y=i" => \$init_y,
    "x=i" => \$init_x,
    "generations=i" => \$gen,
) or die "Error in command line arguments\n";

my @grid;
my ($y, $x) = ($init_y, $init_x);
my $d = 0;

while (<>) {
    next if $_ =~ /^(#|x)/;

    for my $c (split //, $_) {
        given ($c) {
            when (/b|o/) {
                $d = 1 unless $d;
                $grid[$y][$x++] = $c eq 'o' ? 1 : 0 for 1 .. $d;
                $d = 0;
            }
            when ('$') {
                $d = 1 unless $d;
                $y += $d;
                $width = $x + 1 if $x > $width;
                $d = 0;
                $x = $init_x;
            }
            $d = $d * 10 + $c when /\d/;
            last when '!';
        }
    }
}

$height = scalar @grid unless defined $height;

$#grid = $height - 1;
for my $l (@grid) {
    $#$l = $width - 1;
    for my $v (@$l) {
        $v = 0 unless defined $v;
    }
}

print "$height $width\n";
print "@$_\n" for @grid;
print "$gen\n0\n"
