#!/usr/bin/env python3

import bz2
import signal
import subprocess
import sys
import tempfile
import time
import urllib.request

def cleanup(*args):
    sys.stdout.write("\033[2J\033[H\033[?25h\033[m")
    print("Ne nous enlevez pas de points à cause de ça svp")
    sys.exit(0)

with urllib.request.urlopen(
    "http://keroserene.net/lol/astley80.full.bz2",
) as raw_video, tempfile.TemporaryFile() as audio:
    video = bz2.open(raw_video)

    with urllib.request.urlopen(
        "http://keroserene.net/lol/roll.s16",
    ) as data:
        print("Attendez un peu, ça arrive…")
        for d in data:
            audio.write(d)
    audio.seek(0)

    with subprocess.Popen(
        ["aplay", "-Dplug:default", "-q", "-f", "S16_LE", "-r", "16000"],
        stdin=audio,
    ):
        sys.stdout.write("\033[?25l\033[2J\033[H")
        signal.signal(signal.SIGINT, cleanup)
    
        fps = 30
        time_per_frame = 1.0 / fps
        buf = b""
        frame = 0
        next_frame = 0
        begin = time.time()
    
        for i, line in enumerate(video):
            if i % fps == 0:
                frame += 1
                
                sys.stdout.buffer.write(buf)
                buf = b""
                
                elapsed = time.time() - begin
                repose = (frame * time_per_frame) - elapsed
                
                if repose > 0.0:
                    time.sleep(repose)
                    
                next_frame = elapsed / time_per_frame
                
            if frame >= next_frame:
                buf += line
    
cleanup()
