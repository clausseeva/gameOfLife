/* Nécessaire pour l'utilisation de nanosleep */
#define _POSIX_C_SOURCE 200809L

#include <argp.h>
#include <assert.h>
#include <locale.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>

#include "gol.h"
#include "config.h"

/* 2 niveaux de macros pour remplacer la macro avant de stringifier */
#define xstr(s) str(s)
#define str(s) #s

static char doc[] =
    "Lit le motif depuis FICHIER, ou depuis l'entrée standard "
    "si FICHIER est - ou n'est pas défini.";
static char args_doc[] = "[FICHIER]";

static struct argp_option options[] = {
    {"delai", 'd', "DELAI", 0,
     "Le delai entre l'affichage de deux états en ms (défaut : "
     xstr(DELAI_MILISECONDES) ")"},
    {"vivante", 'v', "CARACTERES", 0,
     "Les caractères utilisés pour représenter une cellule vivante (défaut : \""
     SYMBOLE_CELLULE_VIVANTE "\")"},
    {"morte", 'm', "CARACTERES", 0,
     "Les caractères utilisés pour représenter une cellule morte (défaut : \""
     SYMBOLE_CELLULE_MORTE "\")"},
    {0}
};

struct arguments {
    char *fichier;
    unsigned delai;
    char *vivante, *morte;
};

static error_t parse_opt(int key, char *arg, struct argp_state *state) {
    struct arguments *arguments = state->input;

    switch (key) {
    case 'd':
        if (sscanf(arg, "%u", &arguments->delai) != 1)
            argp_error(state, "\"%s\" n'est pas un nombre entier", arg);
        break;

    case 'v':
        arguments->vivante = arg;
        break;
    case 'm':
        arguments->morte = arg;
        break;

    case ARGP_KEY_ARG:
        if (state->arg_num >= 1)
            /* On est au second argument, alors qu'on n'en attend maximum un */
            argp_usage(state);

        arguments->fichier = arg;
        break;

    default:
        return ARGP_ERR_UNKNOWN;
    }

    return 0;
}

static struct argp argp = {options, parse_opt, args_doc, doc};

static void afficher_grille(Grille *g, char *vivante, char *morte) {
    assert(g != NULL);

    for (int y = 0; y < g->hauteur; y++) {
        for (int x = 0; x < g->largeur; x++) {
            if (*g->acceder(g, y, x))
                printf("%s", vivante);
            else
                printf("%s", morte);
        }
        printf("\n");
    }
}

int main(int argc, char **argv) {
    /* Utilise la locale du système (au lieu de la locale 'C' par défaut) */
    setlocale(LC_ALL, "");

    struct arguments arguments = {
        .fichier = "-",
        .delai = DELAI_MILISECONDES,
        .vivante = SYMBOLE_CELLULE_VIVANTE,
        .morte = SYMBOLE_CELLULE_MORTE,
    };

    argp_parse(&argp, argc, argv, 0, 0, &arguments);

    FILE *fp;
    if (!strcmp(arguments.fichier, "-")) {
        fp = stdin;
    } else {
        fp = fopen(arguments.fichier, "r");
        if (!fp) {
            perror("Impossible d'ouvrir le fichier");
            return 1;
        }
    }

    Grille *g = grille_init_fichier(fp);
    if (!g) {
        fprintf(stderr, "Le motif n'a pas pu être lu\n");
        return 1;
    }

    if (fp != stdin)
        fclose(fp);

    struct timespec delai = {
        .tv_sec = arguments.delai / 1000,
        .tv_nsec = (arguments.delai % 1000) * 1000000,
    };

    do {
        /* Remet le curseur en haut à gauche et efface l'écran */
        printf("\033[2J\033[H");

        afficher_grille(g, arguments.vivante, arguments.morte);

        /* On utilise nanosleep au lieu de usleep, car cette dernière est obsolète
           dans le standard POSIX.1-2001 et a été enlevée dans POSIX.1-2008.
        */
        nanosleep(&delai, NULL);
        tour_suivant(g);
    } while (g->tour < g->nb_tours);

    grille_deinit(g);

    return 0;
}
