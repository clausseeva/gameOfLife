#include <stdlib.h>

/* On inclus gol.c et non gol.h, car on veut aussi tester les fonctions
 * déclarées avec static (portée limitée au fichier dans lequel elles sont
 * définies).
 */
#include "gol.c"

#define test(_t, ...) do {                          \
        printf(#_t"...");                           \
        fflush(stdout);                             \
        int _r = (_t)(__VA_ARGS__);                 \
        if (_r) {                                   \
            return 1;                               \
        } else {                                    \
            printf("\033[32m réussite\033[0m\n");   \
        }                                           \
    } while (0)

#define expect(_p, ...) do {                            \
        if (!(_p)) {                                    \
            printf("\033[31m échec\033[0m\n");          \
            printf("%s:%d dans %s: %s a échoué\n",      \
                   __FILE__, __LINE__, __func__, #_p);  \
            printf(__VA_ARGS__);                        \
            printf("\n");                               \
            return 1;                                   \
        }                                               \
    } while (0)

static int test_grille_init(void) {
    Grille *g = grille_init(10, 10, FINIE, 10);
    expect(g != NULL, "g ne devrait pas être null");
    grille_deinit(g);

    g = grille_init(100, 201, TORIQUE, 300);
    expect(g != NULL, "g ne devrait pas être null");
    grille_deinit(g);

    return 0;
}

static int test_acceder_finie(void) {
    Grille *g = grille_init(2, 4, FINIE, 0);
    expect(g != NULL, "g ne devrait pas être null");

    for (int y = -2; y < 4; y++) {
        for (int x = -2; x < 6; x++) {
            Cell *c = g->acceder(g, y, x);
            if (0 <= y && y < 2 && 0 <= x && x < 4)
                expect(c != NULL, "La cellule en %d %d devrait exister", y, x);
            else
                expect(c == NULL, "La cellule en %d %d ne devrait pas exister", y, x);
        }
    }

    grille_deinit(g);
    return 0;
}

static int test_acceder_torique(void) {
    Grille *g = grille_init(4, 3, TORIQUE, 0);
    expect(g != NULL, "g ne devrait pas être null");

    for (int y = 0; y < 4; y++) {
        for (int x = 0; x < 3; x++) {
            Cell *c = g->acceder(g, y, x);
            expect(c != NULL, "La cellule en %d %d devrait exister", y, x);
        }
    }

    const Pos meme_pos[7][2] = {
        {{0, 0}, {4, 3}},
        {{0, 0}, {0, 3}},
        {{0, 0}, {4, 0}},

        {{3, 2}, {-1, -1}},
        {{3, 2}, {3, -1}},
        {{3, 2}, {-1, 2}},

        {{2, 2}, {2, -1}},
    };

    for (int i = 0; i < 7; i++) {
        const Pos *p = meme_pos[i];
        expect(g->acceder(g, p[0].y, p[0].x) == g->acceder(g, p[1].y, p[1].x),
               "La cellule en %d %d devrait être la même qu'en %d %d",
               p[0].y, p[0].x, p[1].y, p[1].x);
    }
               

    grille_deinit(g);
    return 0;
}

static int test_nombre_voisins(void) {
    Grille *g = grille_init(3, 3, FINIE, 4);
    expect(g != NULL, "g ne devrait pas être null");

    expect(nombre_voisins(g, 1, 1) == 0, "la cellule ne devrait pas avoir de voisins");
    *g->acceder(g, 0, 1) = 1;
    expect(nombre_voisins(g, 1, 1) == 1, "la cellule devrait avoir un voisin");
    *g->acceder(g, 1, 1) = 1;
    expect(nombre_voisins(g, 1, 1) == 1, "la cellule devrait avoir un voisin");
    expect(nombre_voisins(g, 0, 0) == 2, "la cellule devrait avoir 2 voisins");

    grille_deinit(g);
    return 0;
}

int main() {
    test(test_grille_init);
    test(test_acceder_finie);
    test(test_acceder_torique);
    test(test_nombre_voisins);
    return 0;
}
