#include <assert.h>
#include <stdlib.h>

#include "gol.h"

#define VOISINS(p) (Pos [8]){                                           \
    {(p).y - 1, (p).x - 1}, {(p).y - 1, (p).x}, {(p).y - 1, (p).x + 1}, \
    {(p).y    , (p).x - 1},                     {(p).y    , (p).x + 1}, \
    {(p).y + 1, (p).x - 1}, {(p).y + 1, (p).x}, {(p).y + 1, (p).x + 1}, \
}

typedef struct {
    int y, x;
} Pos;

static Cell *acceder_finie(Grille *g, int y, int x) {
    assert(g != NULL);
    if (y < 0 || g->hauteur <= y
        || x < 0 || g->largeur <= x)
        return NULL;

    return &g->cellules[y * g->largeur + x];
}

static Cell *acceder_torique(Grille *g, int y, int x) {
    assert(g != NULL);
    int q = (y + g->hauteur) % g->hauteur;
    int p = (x + g->largeur) % g->largeur;
    return &g->cellules[q * g->largeur + p];
}

static int nombre_voisins(Grille *g, int y, int x) {
    assert(g != NULL);

    int res = 0;
    Pos pos_actuelle = {y, x};

    for (int i = 0; i < 8; i++) {
        Pos p = VOISINS(pos_actuelle)[i];
        Cell *c = g->acceder(g, p.y, p.x);
        if (c != NULL && *c & 1)
            res++;
    }

    return res;
}

Grille *grille_init(int hauteur, int largeur, TypeMatrice tm, int nb_tours) {
    assert(hauteur > 0 && largeur > 0);

    Grille *g = calloc(1, sizeof *g);
    if (g == NULL)
        goto err;

    g->hauteur = hauteur;
    g->largeur = largeur;
    g->nb_tours = nb_tours;

    switch (tm) {
    case FINIE:
        g->acceder = &acceder_finie;
        break;
    case TORIQUE:
        g->acceder = &acceder_torique;
        break;
    default:
        goto err;
    }

    g->cellules = calloc(largeur * hauteur, sizeof *g->cellules);
    if (g->cellules == NULL)
        goto err;

    return g;

err:
    if (g != NULL)
        free(g);
    return NULL;
}

Grille *grille_init_fichier(FILE *fp) {
    assert(fp != NULL);

    Grille *g = NULL;
    int hauteur, largeur;
    if (fscanf(fp, "%d %d", &hauteur, &largeur) != 2)
        goto err;
    if (hauteur <= 0 || largeur <= 0)
        goto err;

    g = grille_init(hauteur, largeur, FINIE, 0);
    if (g == NULL)
        goto err;

    for (int y = 0; y < hauteur; y++) {
        for (int x = 0; x < largeur; x++) {
            Cell *c = g->acceder(g, y, x);
            assert(c);
            if (fscanf(fp, "%"SCNu8, c) != 1)
                goto err;
        }
    }

    if (fscanf(fp, "%d", &g->nb_tours) != 1)
        goto err;

    int matrice;
    if (fscanf(fp, "%d", &matrice) != 1)
        goto err;

    switch ((TypeMatrice)matrice) {
    case FINIE:
        break;
    case TORIQUE:
        g->acceder = &acceder_torique;
        break;
    default:
        goto err;
    }

    return g;

err:
    if (g != NULL)
        grille_deinit(g);
    return NULL;
}

void grille_deinit(Grille *g) {
    assert(g != NULL);

    free(g->cellules);
    free(g);
}

void tour_suivant(Grille *g) {
    assert(g != NULL);

    for (int y = 0; y < g->hauteur; y++) {
        for (int x = 0; x < g->largeur; x++) {
            Cell *c = g->acceder(g, y, x);
            int n = nombre_voisins(g, y, x);

            if (n == 3 || (*c & 1 && n == 2))
                *c |= 2;
        }
    }

    for (int i = 0; i < g->hauteur * g->largeur; i++)
        g->cellules[i] >>= 1;

    g->tour++;
}
