/* Ce code utilise des fonctionnalités du C héritées des versions pre-ANSI,
 * donc il vaut mieux désactiver les warnings avant de le compiler sous peine
 * de faire assassiner par le compilateur.
 *
 * L'omission des #include <stdio.h> et #include <stdlib.h> ainsi que
 * la supposition que sizeof(int) == 4 le rendent non portable.
 */

/* Les déclarations dans le top-level des fichiers sont par défaut
 * de type int et de valeur 0.
 */
l;h; /* largeur et hauteur de la grille */
x;y;
*c; /* la grille */
t; /* le nombre de tours */
m; /* le type de la matrice */
i;j;
p;q;
n; /* le nombre de voisins */

main() {
    /* récupération des dimensions */
    scanf("%d%d", &h, &l);

    /* allocation de la grille (sizeof(int) == 4 généralement) */
    c = malloc(l * h * 4);

    /* récupération des valeurs des cellules */
    for (; x<h*l;)
        /* Rappel :
         * x++ renvoie x et l'incrémente après
         * ++x incrémente x et le renvoie ensuite
         */
        scanf("%d", c + x++); /* on sauve un caractère en incrémentant x ici */

    /* Rappel :
     * for (
     *     évalué une fois au début;
     *     évalué à chaque fois avant le corps de la boucle, si 0, la boucle s'arrête;
     *     évalué à chaque fois après le corps de la boucle
     * ) corps de la boucle (instruction ou bloc)
     */
    for (
        scanf("%d%d", &t, &m); /* on sauve un ; en récupérant le nombre de tours et le type de la matrice ici */
        t--; /* on décrémente le nombre de tours jusqu'à ce qu'il arrive à 0 */
        sleep(1) /* 1 seconde de pause après chaque tour */
    ) {
        system("clear"); /* j'ai pas trouvé plus court pour effacer le terminal */

        /* Rappel : l'opérateur , évalue l'expression à gauche
         * puis évalue et renvoie celle à droite.
         */
        /* On itère sur toutes les valeurs du tableau. */
        for(y = 0; y < h; puts("") /* on revient à la ligne */, y++)
            for(x = 0; n = 0, x < l; x++) {
                /* Pour afficher la cellule, on utilise le fait que les caractères
                 * sont des entiers définis par leur code ASCII.
                 * Comme la valeur de la cellule est 0 ou 1 (morte ou vivante),
                 * il suffit d'y ajouter la valeur d'un caractère affichable.
                 * Ici j'ai choisi '.' (46), donc une cellule morte sera affichée
                 * par '.' et une cellule vivante par '/' (47).
                 */
                putchar(c[y * l + x] + 46);
                
                /* On a affiché la cellule, maintenant on peut la modifier.
                 * Pour ne pas interférer avec le calcul des voisins, on stocke
                 * l'état suivant dans le second bit.
                 */

                /* calcul du nombre de voisins */
                for(j = -1; j < 2; j++)
                    for(i = -1; q = y + j, p = x + i, i < 2; i++)
                        /* Priorité des opérateurs (de la plus forte à la plus faible) :
                         *   %, *
                         *   +
                         *   >=, <
                         *   &
                         *   |
                         *   &&
                         *   ||
                         *
                         * & et | peuvent être utilisés à la place de && et || pour 0 et 1
                         * pour éviter de mettre des parenthèses.
                         *
                         * On gère ici les 2 types de matrices.
                         * Bon courage pour comprendre ça :P
                         */
                        n += m | q >= 0 & q < h & p >= 0 & p < l && c[(q + h) % h * l + (p + l) % l] % 2;

                /* On stocke l'état suivant dans le second bit 
                 * On prend en compte le fait que le calcul du nombre de voisins
                 * inclut aussi la cellule elle-même.
                 */
                c[y * l + x] += (n == 3 | c[y * l + x] & n == 4) * 2;
            }

        /* met le second bit dans le premier sur toute la grille */
        for(x = 0; x < h * l;)
            c[x++] /= 2;
    }
}
