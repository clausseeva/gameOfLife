#ifndef CONFIG_H
#define CONFIG_H

/* Ici on peut changer les paramètres par défaut du programme.
 * Ils peuvent être écrasés par les arguments fournis par l'utilisateur.
 */

/* Les caractères affichés dans le terminal qui représentent les cellules. */
#ifndef SYMBOLE_CELLULE_VIVANTE
# define SYMBOLE_CELLULE_VIVANTE "██"
#endif

#ifndef SYMBOLE_CELLULE_MORTE
# define SYMBOLE_CELLULE_MORTE "  "
#endif

/* Le délai en ms entre l'affichage de deux états. */
#ifndef DELAI_MILISECONDES
# define DELAI_MILISECONDES 500
#endif

#endif
