/** @file */

#ifndef GOL_H
#define GOL_H

#include <inttypes.h>
#include <stdio.h>

/**
 * @brief Type utilisé pour représenter une cellule
 *
 * Deux états sont possibles :
 *   - vivant, représenté par un 1
 *   - mort, représenté par un 0
 *
 * On utilise @c uint8_t car c'est le type qui prend le moins de place
 * dans la mémoire (1 octet) tout en étant indexable dans un tableau.
 */
typedef uint8_t Cell;

/**
 * @brief Alias pour #Grille
 */
typedef struct Grille Grille;

/**
 * @brief Type de la fonction pour accéder à la cellule d'une grille
 */
typedef Cell *AccederFn(Grille *grille, int y, int x);

/**
 * @brief Type d'une matrice
 */
typedef enum {
    /**
     * Les voisins des cellules au bord qui sont en dehors de la grille
     * sont considérés comme morts.
     */
    FINIE,

    /**
     * Les voisins des cellules au bord qui devraient être en dehors
     * de la grille sont en fait de l'autre côté de la grille.
     */
    TORIQUE,
} TypeMatrice;

/**
 * @brief Représente une grille de jeu
 *
 * À initialiser avec #grille_init.
 */
struct Grille {
    /**
     * @brief Tableau représentant la grille de cellules
     *
     * Le tableau est constitué de #hauteur lignes de #largeur cellules à la suite.
     */
    Cell *cellules;

    /**
     * @brief Hauteur de la grille de jeu
     */
    int hauteur;

    /**
     * @brief Largeur de la grille de jeu
     */
    int largeur;

    /**
     * @brief Le tour actuel
     *
     * @c 0 pour l'état initial.
     */
    int tour;

    /**
     * @brief Le nombre de tours
     *
     * @c 0 pour seulement l'état initial, @c 1 pour une génération, etc.
     * @c -1 pour pas de limite.
     */
    int nb_tours;

    /**
     * @brief Fonction pour accéder à une cellule de la grille
     *
     * @return Un pointeur vers la cellule, ou @c NULL si elle l'existe pas
     */
    AccederFn *acceder;
};

/**
 * @brief Initialise une grille de jeu
 *
 * La grille de jeu est initialisée sur le tas,
 * il ne faut pas oublier d'appeler #grille_deinit pour libérer la mémoire.
 *
 * @param hauteur Hauteur de la grille de jeu
 * @param largeur Largeur de la grille de jeu
 * @param tm Type de la matrice
 * @param nb_tours Nombre de tours (voir Grille#nb_tours)
 * @return Un pointeur vers la grille initialisée, ou @c NULL en cas d'erreur
 */
Grille *grille_init(int hauteur, int largeur, TypeMatrice tm, int nb_tours);

/**
 * @brief Initialise une grille de jeu depuis un fichier
 *
 * La grille de jeu est initialisée sur le tas,
 * il ne faut pas oublier d'appeler #grille_deinit pour libérer la mémoire.
 *
 * @param fp Pointeur vers le fichier à lire
 * @return Un pointeur vers la grille initialisée, ou @c NULL en cas d'erreur
 */
Grille *grille_init_fichier(FILE *fp);

/**
 * @brief Désinitialise une grille de jeu
 *
 * Crash si @p grille est @c NULL.
 *
 * @param grille La grille à désinitialiser
 */
void grille_deinit(Grille *grille);

/**
 * @brief Avance à l'étape suivante
 *
 * Crash si @p grille est @c NULL.
 *
 * @param grille La grille de jeu à utiliser
 */
void tour_suivant(Grille *grille);

#endif
